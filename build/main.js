"use strict";

var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];

function compareName(a, b) {
  if (a.name < b.name) return -1;
  if (a.name > b.name) return 1;
  return 0;
}

function comparePriceSmall(a, b) {
  return a.price_small - b.price_small;
}

function comparePriceSmallAndLarge(a, b) {
  if (a.price_small == b.price_small) return a.price_large - b.price_large;
  return a.price_small - b.price_small;
}

function filtedBase(_ref) {
  var base = _ref.base;
  return base == 'tomate';
}

function filtedPriceSmall(_ref2) {
  var price_small = _ref2.price_small;
  return price_small < 6;
}

function filtedTwoI(_ref3) {
  var name = _ref3.name;
  return name.split('i').length - 1 == 2;
}

var sortedByName = data.sort(compareName);
var sortedByLittlePrice = data.sort(comparePriceSmall);
var sortedByLittlePriceAndBigPrice = data.sort(comparePriceSmallAndLarge);
var filtedByBase = data.filter(filtedBase);
var filtedByLittlePriceLessThanSix = data.filter(filtedPriceSmall);
var filtedByTwoI = data.filter(filtedTwoI);

function constructHtml(_ref4) {
  var name = _ref4.name,
      price_small = _ref4.price_small,
      price_large = _ref4.price_large,
      image = _ref4.image;
  return "<article class=\"pizzaThumbnail\">\n\t\t\t<a href=\"".concat(image, "\">\n                <img src=\"").concat(image, "\"/>\n                <section>\n                    <h4>").concat(name, "</h4>\n                    <ul>\n                        <li>Prix petit format : ").concat(price_small, " \u20AC</li>\n                        <li>Prix grand format : ").concat(price_large, " \u20AC</li>\n                    </ul>\n                </section>\n\t\t\t</a>\n\t\t\t</article>");
}

sortedByName.forEach(function (e) {
  document.querySelector('.pageContent').innerHTML += constructHtml(e);
});
//# sourceMappingURL=main.js.map