const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];


function compareName(a, b) {
    if (a.name < b.name) return -1;
    if (a.name > b.name) return 1;
    return 0;
}

function comparePriceSmall(a, b) {
    return a.price_small - b.price_small;
}

function comparePriceSmallAndLarge(a, b) {
    if (a.price_small == b.price_small) return a.price_large - b.price_large;
    return a.price_small - b.price_small;
}

function filtedBase({base}) {
    return base == 'tomate';
}

function filtedPriceSmall({price_small}) {
    return price_small < 6;
}

function filtedTwoI({name}) {
    return name.split('i').length - 1 == 2;
}

const sortedByName = data.sort(compareName);
const sortedByLittlePrice = data.sort(comparePriceSmall);
const sortedByLittlePriceAndBigPrice = data.sort(comparePriceSmallAndLarge);

const filtedByBase = data.filter(filtedBase);
const filtedByLittlePriceLessThanSix = data.filter(filtedPriceSmall);
const filtedByTwoI = data.filter(filtedTwoI);

function constructHtml({name, price_small, price_large, image}) {
	return `<article class="pizzaThumbnail">
			<a href="${image}">
                <img src="${image}"/>
                <section>
                    <h4>${name}</h4>
                    <ul>
                        <li>Prix petit format : ${price_small} €</li>
                        <li>Prix grand format : ${price_large} €</li>
                    </ul>
                </section>
			</a>
			</article>`;
}

sortedByName.forEach(e=>{
    document.querySelector('.pageContent').innerHTML += constructHtml(e);
});
